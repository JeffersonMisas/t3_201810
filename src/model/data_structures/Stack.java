package model.data_structures;

public interface Stack<T> {

	/**
	 * Agrega un item al tope de la lista.
	 * @param elemento
	 */
	public void push( T elemento );
	
	/**
	 * Elimina el elemento en el tope de la lista.
	 */
	public T pop( );
	
	/**
	 * Indica si la pila esta vacia.
	 * @return true si la pila esta vacia, false en caso contrario.
	 */
	public boolean isEmpty( );
	
	/**
	 * Devuelve el tama�o de la pila.
	 * @return el n�mero de elementos en la pila.
	 */
	public int size( );
}