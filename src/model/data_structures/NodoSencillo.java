package model.data_structures;

public class NodoSencillo<E> {
	
    private NodoSencillo<E> siguiente;
    private E elemento;
    
    public NodoSencillo(E pElemento) {
    	
    	siguiente = null;
    	this.elemento = pElemento;
    }
    
    public NodoSencillo<E> darSiguiente( ){
    	
    	return siguiente;
    	
    }
    
    public void cambiarSiguiente( NodoSencillo<E> pSiguiente ){
    	
    	this.siguiente = pSiguiente;
    }
    
    public E darElemento( ){
    	
    	return elemento;
    }
    
    public void cambiarElemento( E pElemento){
    	
    	this.elemento = pElemento;
    }
    
}
