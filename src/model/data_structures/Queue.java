package model.data_structures;

public interface Queue<T> {

	/**
	 * Agrega un item en la �ltima posici�n de la cola.
	 * @param elemento
	 */
	public void enqueue( T elemento );
	
	/**
	 * Elimina el elemento en la ultima posici�n de la cola.
	 */
	public T dequeue( );
	
	/**
	 * Indica si la pila esta vacia.
	 * @return true si la pila esta vacia, false en caso contrario.
	 */
	public boolean isEmpty( );
	
	/**
	 * Devuelve el tama�o de la pila.
	 * @return el n�mero de elementos en la pila.
	 */
	public int size( );
}
