package model.data_structures;

import java.util.EmptyStackException;

public class ListaEncadenadaStack<T> implements Stack<T>{

    private NodoSencillo<T> topStack;
    
	private int size = 0;
	
	
	public ListaEncadenadaStack( ) {
		topStack = null;
	}

	@Override
	public void push(T elemento) {
		// TODO Auto-generated method stub
		NodoSencillo<T> nuevoNodo = new NodoSencillo<T>(elemento);
		
		if( topStack == null ){
			topStack = nuevoNodo;
		}
		else{
			nuevoNodo.cambiarSiguiente( topStack );
			topStack = nuevoNodo;
		}
		size++;
	}

	@Override
	public T pop() {
		// TODO Auto-generated method stub
		if( topStack == null ){
			throw new EmptyStackException( );
		}
		T elemento = topStack.darElemento( );
		NodoSencillo<T> nextTop = topStack.darSiguiente( );
		topStack.cambiarSiguiente(null);
		topStack = nextTop;
		size--;
		return elemento;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if (size == 0){
			return true;
		}
		else{
			return false;
		}
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

}
