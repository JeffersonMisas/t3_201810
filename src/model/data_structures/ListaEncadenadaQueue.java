package model.data_structures;

import java.util.EmptyStackException;

public class ListaEncadenadaQueue<T> implements Queue<T> {

	private NodoSencillo<T> first;
	
	private NodoSencillo<T> last;
	
	private int size = 0;
	
	
	@Override
	public void enqueue(T elemento) {
		// TODO Auto-generated method stub
		NodoSencillo<T> nuevoNodo = new NodoSencillo<T>(elemento);
		
		if( size == 0 ){
			first  = nuevoNodo;
			last = nuevoNodo;
		}
		else{
			NodoSencillo<T> oldLast = last;
			oldLast.cambiarSiguiente(nuevoNodo);
			last = nuevoNodo;
		}
		size++;
	}

	@Override
	public T dequeue() {
		// TODO Auto-generated method stub
		if(size == 0){
			throw new EmptyStackException();
		}
		NodoSencillo<T> oldFirst = first;
		T elemento = first.darElemento();
		first = oldFirst.darSiguiente();
		oldFirst.cambiarSiguiente(null);
		size--;
		return elemento;
		
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if (size == 0){
			return true;
		}
		else{
			return true;
		}
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

}
